package enc01;

import java.io.IOException;
import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;

public class Ex3 {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);

        while (true) {
            int N = sc.nextInt();
            if (N == 0)
                break;

            while (true) {
                Stack<Integer> saida = new Stack<>();

                int pr = sc.nextInt();
                if (pr == 0)
                    break;
                saida.push(pr);

                for (int i = 1; i < N; i++) {
                    saida.push(sc.nextInt());
                }

                Collections.reverse(saida);
                Stack<Integer> stack = new Stack<>();

                for (int i = 1; i <= N; ++i) {
                    stack.push(i);

                    while (!stack.empty() && stack.peek().equals(saida.peek())) {
                        stack.pop();
                        saida.pop();
                    }
                }
                System.out.println(stack.empty() ? "Yes" : "No");
            }

            System.out.println("");
        }
    }
}