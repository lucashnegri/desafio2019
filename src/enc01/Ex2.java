package enc01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<Integer> numeros = new ArrayList<>();

        while(true) {
            int num = sc.nextInt();
            if(num == 0) {
                break;
            }
            numeros.add(num);
        }

        Collections.reverse(numeros);

        for(int n: numeros) {
            System.out.println(n);
        }
    }
}
