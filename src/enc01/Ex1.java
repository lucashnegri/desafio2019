package enc01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> nomes = new ArrayList<>();

        while(true) {
            String nome = sc.nextLine();
            if(nome.equals("FIM")) {
                break;
            }
            nomes.add(nome);
        }

        Collections.sort(nomes);

        for(String n: nomes) {
            System.out.println(n);
        }
    }
}
