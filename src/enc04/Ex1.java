package enc04;

import java.util.Scanner;

public class Ex1 {
    public static int INF = 1 << 29;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();

        // cria matriz de adjacências
        int[][] dist = new int[N][N];

        // distância inicial é infinita
        for(int i = 0; i < N; ++i)
            for(int j = 0; j < N; ++j)
                dist[i][j] = INF;

        // lê a entrada
        int M = sc.nextInt();
        for(int m = 0; m < M; ++m) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            int c = sc.nextInt();
            // grafo direcionado!
            dist[a][b] = c;
        }

        // calcula o menor caminho utilizando o método de Floyd-Warshall
        for(int k = 0; k < N; ++k)
            for(int i = 0; i < N; ++i)
                for(int j = 0; j < N; ++j)
                    if(dist[i][j] > dist[i][k] + dist[k][j])
                        dist[i][j] = dist[i][k] + dist[k][j];

        // lê e responde perguntas
        int Q = sc.nextInt();
        for(int q = 0; q < Q; ++q) {
            int a = sc.nextInt();
            int b = sc.nextInt();
            if(dist[a][b] == INF)
                System.out.printf("Não há caminho entre %d e %d\n", a, b);
            else
                System.out.printf("A distância entre %d e %d é %d\n", a, b, dist[a][b]);
        }
    }
}
