/**
 * Solução utilizando busca em profundidade.
 * lhn, 2018
 */
package enc06;


import java.util.Scanner;
import java.util.Stack;

class Posicao {
    int x;
    int y;

    Posicao(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

public class Contaminacao {
    public static void main(String[] args) {
        Scanner leitor = new Scanner(System.in);
        while (true) {
            int N, M;
            N = leitor.nextInt();
            M = leitor.nextInt();
            if (N == 0 && M == 0) break;
            leitor.nextLine(); // descarta pulo de linha

            char[][] mapa = new char[N][M];

            // leitura
            for (int i = 0; i < N; ++i) {
                String linha = leitor.nextLine();
                linha.getChars(0, M, mapa[i], 0);
            }

            // flood fill (pontos iniciais)
            Stack<Posicao> pilha = new Stack<>();
            for (int i = 0; i < N; ++i) {
                for (int j = 0; j < M; ++j) {
                    if (mapa[i][j] == 'T') {
                        pilha.push(new Posicao(i, j));
                        mapa[i][j] = 'A';
                    }
                }
            }

            while (!pilha.empty()) {
                Posicao pos = pilha.pop();
                if (pos.x < 0 || pos.x >= N || pos.y < 0 || pos.y >= M)
                    continue;

                if (mapa[pos.x][pos.y] == 'A') {
                    mapa[pos.x][pos.y] = 'T';
                    pilha.push(new Posicao(pos.x + 1, pos.y));
                    pilha.push(new Posicao(pos.x - 1, pos.y));
                    pilha.push(new Posicao(pos.x, pos.y + 1));
                    pilha.push(new Posicao(pos.x, pos.y - 1));
                }
            }

            for (int i = 0; i < N; ++i) {
                for (int j = 0; j < M; ++j) {
                    System.out.print(mapa[i][j]);
                }
                System.out.println();
            }
            System.out.println();
        }
    }
}
