/**
 * Solução utilizando busca em largura.
 * lhn, 2018
 */

package enc05;

import java.util.LinkedList;
import java.util.Scanner;

class Posicao {
    final int l;
    final int c;
    final int passos;

    Posicao(int l, int c, int passos) {
        this.l = l;
        this.c = c;
        this.passos = passos;
    }
}

public class Duende {
    private final static int INF = 1 << 30;
    private final static int[] D = {-1, 0, 0, 1, 1, 0, 0, -1};

    private static boolean incluso(int min, int max, int valor) {
        return valor >= min && valor < max;
    }

    public static void main(String[] args) {
        Scanner leitor = new Scanner(System.in);
        int L, C;
        L = leitor.nextInt();
        C = leitor.nextInt();

        int[][] mapa = new int[L][C];
        boolean[][] visitou = new boolean[L][C];

        LinkedList<Posicao> lista = new LinkedList<>();

        for(int l = 0; l < L; ++l) {
            for(int c = 0; c < C; ++c) {
                mapa[l][c] = leitor.nextInt();
                if(mapa[l][c] == 3) {
                    lista.add(new Posicao(l, c, 0));
                }
            }
        }

        int resposta = INF; // não tem saída!

        while(!lista.isEmpty()) {
            Posicao atual = lista.removeFirst();
            if(visitou[atual.l][atual.c])
                continue;

            visitou[atual.l][atual.c] = true;

            // visitação
            if(mapa[atual.l][atual.c] == 0) {
                // chegou!
                resposta = atual.passos;
                break;
            }

            // gera adjacências
            for(int i = 0; i < 8; i += 2) {
                int cl = atual.l + D[i];
                int cc = atual.c + D[i+1];
                if(incluso(0, L, cl) && incluso(0, C, cc) && mapa[cl][cc] != 2) {
                    lista.add(new Posicao(cl, cc, atual.passos + 1));
                }
            }

        }

        System.out.println(resposta);
    }
}
