package enc02;

import java.util.HashMap;
import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        int M;
        Scanner sc = new Scanner(System.in);
        M = sc.nextInt();

        for(int m = 0; m < M; ++m) {
            HashMap<String, Double> mapa = new HashMap<>();

            int N = sc.nextInt();
            for(int n = 0; n < N; ++n) {
                String nome = sc.next();
                double valor = sc.nextDouble();
                mapa.put(nome, valor);
            }

            int P = sc.nextInt();
            double total = 0;
            for(int p = 0; p < P; ++p) {
                String nome = sc.next();
                double valor = sc.nextDouble();
                total += mapa.get(nome) * valor;
            }

            System.out.printf("R$ %.2f\n", total);
        }
    }
}
