package enc02;

import java.util.HashMap;
import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        HashMap<String, Integer> matriculas = new HashMap<>();
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();

        for(int n = 0; n < N; ++n) {
            String nome = sc.next();
            int idade = sc.nextInt();
            matriculas.put(nome, idade);
        }

        int M = sc.nextInt();
        for(int m = 0; m < M; ++m) {
            String nome = sc.next();
            Integer idade = matriculas.get(nome);
            if(idade != null) {
                System.out.println(idade + " anos");
            } else {
                System.out.println("Não matriculado");
            }
        }
    }
}
