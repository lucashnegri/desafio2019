package enc03;

import java.util.Scanner;

/**
 *
 * @author Fernando
 */
public class UmDoisTres {

    public static void main(String[] args) {
        
        int qtdePalavras;
        String w;
        String[] numbers = new String[3], words;
        numbers[0] = "one-1";
        numbers[1] = "two-2";
        numbers[2] = "three-3";
        
        Scanner scanner = new Scanner(System.in);        
        
        do{
            System.out.println("Insira o número de palavras que o seu irmão mais novo escreveu:");
            qtdePalavras = scanner.nextInt();
        }
        while(qtdePalavras <= 0 || qtdePalavras > 1000);
        
        words = new String[qtdePalavras];
        
        for(int i=0; i<qtdePalavras; i++){
            System.out.println("Insira a "+ (i+1) +"ª palavra que o seu irmão mais novo escreveu:");
            w = scanner.next();
            w = w.toLowerCase();
            words[i] = w;
        }
       
        for (String word : words) {
            for (String number : numbers) {  
                String[] n1;
                n1 = number.split("-");
     
                if(n1[0].length() == word.length()) {
                    int cError = 0;
                    for(int j=0; j<word.length(); j++){   
                        if(n1[0].charAt(j) != word.charAt(j)){
                            cError++; 
                        }
                    }
                    if(cError <= 1){
                        System.out.println("" + n1[1]);
                    }
                }
            }
        }
    }
}
