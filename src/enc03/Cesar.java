package enc03;

import java.util.Scanner;

/**
 *
 * @author Fernando
 */
public class Cesar {
    
    public static void main(String[] args) {
        
        Scanner scanner = new Scanner(System.in);
        int testes,pulo;
        String encrypted, decrypted = "", alpha;
        alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        
        System.out.println("Insira a qtde de testes");
        testes = scanner.nextInt();
        
        for(int j=0; j<testes; j++){
           
           do{
               System.out.println("Insira a string à ser descodificada:");
               encrypted = scanner.next();
               encrypted = encrypted.toUpperCase();
           }
           while(encrypted.length() > 50);
           
           do{
               System.out.println("Insira o pulo usado:");
               pulo = scanner.nextInt();
           }
           while(pulo < 0 || pulo > 25);
               
           for(int i=0; i<encrypted.length(); i++){
               char c = encrypted.charAt(i);
               String ch = Character.toString(c);
               int ind = alpha.indexOf(ch);
               
               int newPos = ind-pulo;
               
               if (newPos < 0){
                   newPos = newPos + 26;
               }
               
               decrypted += Character.toString(alpha.charAt(newPos));
           }
           
           decrypted += "\n";  
        }
        System.out.println(decrypted);         
    }
}
