package obi2019.fase1;

import java.util.Scanner;
import java.util.Stack;

class Ponto {
    int l, c;

    Ponto(int l, int c) {
        this.l = l;
        this.c = c;
    }
}

public class Chuva {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int L = sc.nextInt();
        int C = sc.nextInt();

        // ler a matriz
        char[][] mtx = new char[L][];
        for (int l = 0; l < L; l++) {
            mtx[l] = sc.next().toCharArray();
        }

        // encontrar o ponto inicial
        boolean achou = false;
        Stack<Ponto> pilha = new Stack<>();

        for (int l = 0; l < L && !achou; ++l) {
            for (int c = 0; c < C; ++c) {
                if (mtx[l][c] == 'o') {
                    pilha.push(new Ponto(l, c));
                    mtx[l][c] = '.';
                    achou = true;
                    break;
                }
            }
        }

        // propagar a água
        while (!pilha.empty()) {
            Ponto p = pilha.pop();

            if (mtx[p.l][p.c] == 'o') {
                // já visitou, não precisa processar
                continue;
            }

            mtx[p.l][p.c] = 'o';

            if (p.l + 1 < L) {
                char abaixo = mtx[p.l + 1][p.c];

                if (abaixo == '.') {
                    // se abaixo estiver livre, propaga para baixo
                    pilha.push(new Ponto(p.l + 1, p.c));
                } else if (abaixo == '#') {
                    // se houver uma prateleira abaixo, propaga para os lados
                    pilha.push(new Ponto(p.l, p.c - 1));
                    pilha.push(new Ponto(p.l, p.c + 1));
                }
            }
        }

        // imprimir o resultado
        for (int l = 0; l < L; ++l) {
            System.out.println(String.valueOf(mtx[l]));
        }
    }
}