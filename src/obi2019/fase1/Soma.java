package obi2019.fase1;

import java.util.Scanner;

public class Soma {
    public static void main(String[] args) {
        Scanner leitor = new Scanner(System.in);
        int N = leitor.nextInt();
        int K = leitor.nextInt();

        int[] V = new int[N];
        for (int i = 0; i < N; ++i)
            V[i] = leitor.nextInt();

        long total = 0;
        for (int i = 0; i < N; i++) {
            int soma = 0;

            for (int j = i; j < N; ++j) {
                soma += V[j];
                if (soma == K) {
                    ++total;
                } else if (soma > K) {
                    break;
                }
            }
        }

        System.out.println(total);
    }
}