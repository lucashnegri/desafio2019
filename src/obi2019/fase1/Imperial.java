package obi2019.fase1;

/*
 Estratégia: verificação das sequências na força bruta.
 Dois ponteiros (pa e pb) são movidos de forma intercalada, seguindo as restrições do problema.
 Complexidade: O(N³)
*/

import java.util.Scanner;

public class Imperial {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();

        int V[] = new int[N];
        for (int i = 0; i < N; ++i)
            V[i] = sc.nextInt();

        int maior = 1;

        for (int i = 0; i < N; ++i)
            for (int j = i + 1; j < N; ++j) {
                int a = V[i];
                int b = V[j];
                if (a == b)
                    continue;

                int total = 2;
                int pa = j + 1;
                int pb = j;

                boolean fim = false;
                while (!fim) {
                    while (pa < N && V[pa] != a) {
                        ++pa;
                    }
                    if (pa == N) {
                        fim = true;
                        break;
                    } else {
                        ++total;
                        ++pa;
                        pb = pa + 1;
                        while (pb < N && V[pb] != b) {
                            ++pb;
                        }
                        if (pb == N) {
                            fim = true;
                            break;
                        } else {
                            ++pb;
                            ++total;
                        }
                    }
                }

                if (total > maior)
                    maior = total;
            }

        System.out.println(maior);
    }
}